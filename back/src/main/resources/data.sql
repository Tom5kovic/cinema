INSERT INTO adress (id, street, number) VALUES (1,'Bulevar Cara Lazara', 5);
INSERT INTO adress (id, street, number) VALUES (2, 'Dalmatinska', 7);

INSERT INTO user (id, e_mail, username, password, first_name, last_name, user_role, adress_id)
              VALUES (1,'miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','Miroslav','Simic','ADMIN',1);
INSERT INTO user (id, e_mail, username, password, first_name, last_name, user_role, adress_id)
              VALUES (2,'tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','Tamara','Milosavljevic','USER',2);
INSERT INTO user (id, e_mail, username, password, first_name, last_name, user_role, adress_id)
              VALUES (3,'petar@maildrop.cc','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','Petar','Jovic','USER',2);
              
INSERT INTO movie (id, name, duration) VALUES (1, 'Avengers: Endgame', 182);
INSERT INTO movie (id, name, duration) VALUES (2, 'Life', 110);
INSERT INTO movie (id, name, duration) VALUES (3, 'It: Chapter 2', 170);
INSERT INTO movie (id, name, duration) VALUES (4, 'Pirates of the Caribbean: Dead Men Tell No Tales', 153);
INSERT INTO movie (id, name, duration) VALUES (5, 'No country for old man', 123);
INSERT INTO movie (id, name, duration) VALUES (6, 'Hobbit 2', 203);
INSERT INTO movie (id, name, duration) VALUES (7, 'Armagedon', 113);

INSERT INTO hall (id, name) VALUES (1, 1);
INSERT INTO hall (id, name) VALUES (2, 2);
INSERT INTO hall (id, name) VALUES (3, 3);

INSERT INTO seat (id, hall_id) VALUES (1, 1);
INSERT INTO seat (id, hall_id) VALUES (2, 1);
INSERT INTO seat (id, hall_id) VALUES (3, 1);
INSERT INTO seat (id, hall_id) VALUES (4, 1);
INSERT INTO seat (id, hall_id) VALUES (5, 1);
INSERT INTO seat (id, hall_id) VALUES (6, 1);
INSERT INTO seat (id, hall_id) VALUES (7, 2);
INSERT INTO seat (id, hall_id) VALUES (8, 2);
INSERT INTO seat (id, hall_id) VALUES (9, 2);
INSERT INTO seat (id, hall_id) VALUES (10, 2);
INSERT INTO seat (id, hall_id) VALUES (11, 3);
INSERT INTO seat (id, hall_id) VALUES (12, 3);
INSERT INTO seat (id, hall_id) VALUES (13, 3);
INSERT INTO seat (id, hall_id) VALUES (14, 3);
INSERT INTO seat (id, hall_id) VALUES (15, 3);
INSERT INTO seat (id, hall_id) VALUES (16, 3);

INSERT INTO projection (id, date_time, movie_id, hall_id, type, ticket_price) VALUES (1, '2020-06-21 20:00', 1, 1, '2D', 380.00);
INSERT INTO projection (id, date_time, movie_id, hall_id, type, ticket_price) VALUES (2, '2020-07-22 21:00', 2, 2, '3D', 580.00);
INSERT INTO projection (id, date_time, movie_id, hall_id, type, ticket_price) VALUES (3, '2020-06-22 20:00', 1, 3, '4D', 780.00);
INSERT INTO projection (id, date_time, movie_id, hall_id, type, ticket_price) VALUES (4, '2020-08-10 18:00', 3, 1, '2D', 350.00);
INSERT INTO projection (id, date_time, movie_id, hall_id, type, ticket_price) VALUES (5, '2020-08-12 19:00', 4, 2, '4D', 680.00);
INSERT INTO projection (id, date_time, movie_id, hall_id, type, ticket_price) VALUES (6, '2020-11-26 19:00', 6, 3, '4D', 680.00);
INSERT INTO projection (id, date_time, movie_id, hall_id, type, ticket_price) VALUES (7, '2020-10-26 19:00', 5, 2, '4D', 680.00);

INSERT INTO ticket (id, projection_id, seat_id, date_time, user_id) VALUES (1, 1, 1,'2020-06-21 10:00', 1);
INSERT INTO ticket (id, projection_id, seat_id, date_time, user_id) VALUES (2, 2, 2,'2020-06-21 11:00', 2);
              
INSERT INTO genre (id, name) VALUES (1, 'science fiction');
INSERT INTO genre (id, name) VALUES (2, 'action');
INSERT INTO genre (id, name) VALUES (3, 'comedy');
INSERT INTO genre (id, name) VALUES (4, 'horror');
INSERT INTO genre (id, name) VALUES (5, 'adventure');

INSERT INTO movie_genre (movie_id, genre_id) VALUES (1, 1);
INSERT INTO movie_genre (movie_id, genre_id) VALUES (1, 2);
INSERT INTO movie_genre (movie_id, genre_id) VALUES (1, 5);

INSERT INTO movie_genre (movie_id, genre_id) VALUES (2, 1);
INSERT INTO movie_genre (movie_id, genre_id) VALUES (2, 4);

INSERT INTO movie_genre (movie_id, genre_id) VALUES (3, 4);

INSERT INTO movie_genre (movie_id, genre_id) VALUES (4, 2);
INSERT INTO movie_genre (movie_id, genre_id) VALUES (4, 3);
INSERT INTO movie_genre (movie_id, genre_id) VALUES (4, 5);