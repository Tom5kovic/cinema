package com.ftninformatika.jwd.modul3.cinema.support;

import com.ftninformatika.jwd.modul3.cinema.model.Genre;
import com.ftninformatika.jwd.modul3.cinema.service.GenreService;
import com.ftninformatika.jwd.modul3.cinema.web.dto.GenreDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.core.convert.converter.Converter;

@Component
public class GenreDtoToGenre implements Converter<GenreDTO, Genre> {

    @Autowired
    private GenreService service;

    @Override
    public Genre convert(GenreDTO dto) {
        Genre genre;

        if(dto.getId() == null){
            genre = new Genre();
        }else {
            genre = service.findOne(dto.getId());
        }

        if(genre != null){
            genre.setName(dto.getName());
        }
        return genre;
    }
}
