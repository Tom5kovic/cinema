package com.ftninformatika.jwd.modul3.cinema.service.impl;

import com.ftninformatika.jwd.modul3.cinema.model.Movie;
import com.ftninformatika.jwd.modul3.cinema.repository.MovieRepository;
import com.ftninformatika.jwd.modul3.cinema.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class JpaMovieService implements MovieService {

    @Autowired
    private MovieRepository movieRepository;

    @Override
    public Movie findOne(Long id) {
        return movieRepository.findOneById(id);
    }

    @Override
    public List<Movie> findAll() {
        return movieRepository.findAll();
    }

    @Override
    public Movie save(Movie movie) {
        return movieRepository.save(movie);
    }

    @Override
    public Movie update(Movie movie) {
        return movieRepository.save(movie);
    }

    @Override
    public Movie delete(Long id) {
        Optional<Movie> movie = movieRepository.findById(id);
        if(movie.isPresent()){
            movieRepository.deleteById(id);
            return movie.get();
        }
        return null;
    }

    @Override
    public List<Movie> find(String name, Long genreId, Integer durationFrom, Integer durationTo) {
        if (name == null) {
            name = "";
        }

        if (durationFrom == null) {
            durationFrom = 0;
        }

        if (durationTo == null) {
            durationTo = Integer.MAX_VALUE;
        }

        if(genreId == null){
            return movieRepository.findByNameIgnoreCaseContainsAndDurationBetween(name,durationFrom,durationTo);
        }
        return movieRepository.findByNameIgnoreCaseContainsAndDurationBetweenAndGenresId(name,durationFrom,durationTo,genreId);
    }

    @Override
    public List<Movie> findByGenreId(Long genreId) {
        return movieRepository.findByGenresId(genreId);
    }
}
