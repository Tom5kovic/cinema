package com.ftninformatika.jwd.modul3.cinema.web.controller;

import com.ftninformatika.jwd.modul3.cinema.model.Movie;
import com.ftninformatika.jwd.modul3.cinema.model.Projection;
import com.ftninformatika.jwd.modul3.cinema.service.MovieService;
import com.ftninformatika.jwd.modul3.cinema.service.ProjectionService;
import com.ftninformatika.jwd.modul3.cinema.support.MovieDtoToMovie;
import com.ftninformatika.jwd.modul3.cinema.support.MovieToMovieDto;
import com.ftninformatika.jwd.modul3.cinema.support.ProjectionToProjectionDto;
import com.ftninformatika.jwd.modul3.cinema.web.dto.MovieDTO;
import com.ftninformatika.jwd.modul3.cinema.web.dto.ProjectionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Positive;
import java.util.List;

@RestController
@RequestMapping(value = "/api/movies", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class MovieController {

    @Autowired
    private MovieService movieService;

    @Autowired
    private ProjectionService projectionService;

    @Autowired
    private MovieDtoToMovie toMovie;

    @Autowired
    private MovieToMovieDto toMovieDto;

    @Autowired
    private ProjectionToProjectionDto toProjectionDto;

    //@PreAuthorize("hasRole('ADMIN')")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MovieDTO> create(@Valid @RequestBody MovieDTO dto){
        Movie movie = toMovie.convert(dto);
        Movie saved = movieService.save(movie);

        return new ResponseEntity<>(toMovieDto.convert(saved), HttpStatus.CREATED);
    }

    //@PreAuthorize("hasRole('ADMIN')")
    @PutMapping(value = "/{id}",consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MovieDTO> update(@PathVariable Long id, @Valid @RequestBody MovieDTO dto){

        if(!id.equals(dto.getId())) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Movie movie = toMovie.convert(dto);
        Movie saved = movieService.update(movie);

        return new ResponseEntity<>(toMovieDto.convert(saved),HttpStatus.OK);
    }

    //@PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id){
        Movie deleted = movieService.delete(id);

        if(deleted != null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
    @GetMapping("/{id}")
    public ResponseEntity<MovieDTO> getOne(@PathVariable Long id){
       Movie movie = movieService.findOne(id);

        if(movie != null) {
            return new ResponseEntity<>(toMovieDto.convert(movie), HttpStatus.OK);
        }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
    @GetMapping
    public ResponseEntity<List<MovieDTO>> getAll(
            @RequestParam(required=false) String name,
            @RequestParam(required=false) Long genreId,
            @RequestParam(required=false) Integer durationFrom,
            @RequestParam(required=false) Integer durationTo){

        List<Movie> movies = movieService.find(name,genreId,durationFrom,durationTo);

        return new ResponseEntity<>(toMovieDto.convert(movies), HttpStatus.OK);
    }

    //@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
    @GetMapping("/{id}/projections")
    public ResponseEntity<List<ProjectionDTO>> findByMovieId(@PathVariable @Positive(message = "Id must be positive.")  Long id){
        List<Projection> projection = projectionService.findByMovieId(id);

        return new ResponseEntity<>(toProjectionDto.convert(projection), HttpStatus.OK);
    }

}
