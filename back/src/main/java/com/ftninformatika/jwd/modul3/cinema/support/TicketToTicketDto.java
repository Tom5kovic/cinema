package com.ftninformatika.jwd.modul3.cinema.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul3.cinema.model.Ticket;
import com.ftninformatika.jwd.modul3.cinema.web.dto.TicketDTO;

@Component
public class TicketToTicketDto implements Converter<Ticket, TicketDTO>{

	@Autowired
	private ProjectionToProjectionDto toProjectionDto;
	
	@Autowired
	private SeatToSeatDto toSeatDto;
	
	@Autowired
	private UserToUserDto toUserDto;
	
	@Override
	public TicketDTO convert(Ticket t) {
		TicketDTO dto = new TicketDTO();
		
		dto.setId(t.getId());
		dto.setDateTime(t.getDateTime().toString());
		dto.setProjection(toProjectionDto.convert(t.getProjection()));
		dto.setSeat(toSeatDto.convert(t.getSeat()));
		dto.setUser(toUserDto.convert(t.getUser()));
		return dto;
	}
	
	public List<TicketDTO> convert(List<Ticket> tickets){
        List<TicketDTO> dtos = new ArrayList<>();

        for(Ticket t : tickets) {
        	TicketDTO dto = convert(t);
            dtos.add(convert(t));
        }

        return dtos;
    }

}
