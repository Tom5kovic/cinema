package com.ftninformatika.jwd.modul3.cinema.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import com.ftninformatika.jwd.modul3.cinema.model.Seat;
import com.ftninformatika.jwd.modul3.cinema.repository.SeatRepository;
import com.ftninformatika.jwd.modul3.cinema.service.SeatService;

@Service
public class JpaSeatService implements SeatService {

	@Autowired
	private SeatRepository repository;

	@Override
	public Seat findOne(Long id) {
		return repository.findOneById(id);
	}

	@Override
	public List<Seat> findAll() {
		return repository.findAll();
	}

	@Override
	public Seat save(Seat seat) {
		return repository.save(seat);
	}

	@Override
	public Seat delete(Long id) {
		Optional<Seat> seat = repository.findById(id);
        if(seat.isPresent()){
            repository.deleteById(id);
            return seat.get();
        }
        return null;
	}

}
