package com.ftninformatika.jwd.modul3.cinema.web.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.HashSet;
import java.util.Set;

public class MovieDTO {

    @Positive(message = "Id need to be positive.")
    private Long id;

    @NotBlank(message = "Movie must have name.")
    private String name;

    @NotNull(message = "Movie must have duration.")
    @Positive(message = "Duration can't be negative.")
    private int duration;

    @NotEmpty(message = "Movie must have at least one genre.")
    private Set<GenreDTO> genres = new HashSet<>();

    public MovieDTO() {
    }

    public Long getId() {
        return id;
    }

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public Set<GenreDTO> getGenres() {
		return genres;
	}

	public void setGenres(Set<GenreDTO> genres) {
		this.genres = genres;
	}

   
}
