package com.ftninformatika.jwd.modul3.cinema.support;

import java.util.ArrayList;
import java.util.List;

import com.ftninformatika.jwd.modul3.cinema.model.Adress;
import com.ftninformatika.jwd.modul3.cinema.web.dto.AdressDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;


@Component
public class AdressToAdressDto implements Converter<Adress, AdressDTO> {

    @Override
    public AdressDTO convert(Adress adress) {
        AdressDTO adressDto = new AdressDTO();
        adressDto.setId(adress.getId());
        adressDto.setNumber(adress.getNumber());
        adressDto.setStreet(adress.getStreet());
        return adressDto;
    }

    public List<AdressDTO> convert(List<Adress> adresses){
        List<AdressDTO> adressesDto = new ArrayList<>();

        for(Adress a : adresses) {
            AdressDTO dto = convert(a);
            adressesDto.add(dto);
        }

        return adressesDto;
    }

}