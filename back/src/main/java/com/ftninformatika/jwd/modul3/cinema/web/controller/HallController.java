package com.ftninformatika.jwd.modul3.cinema.web.controller;

import java.util.List;

import javax.validation.constraints.Positive;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.modul3.cinema.model.Hall;
import com.ftninformatika.jwd.modul3.cinema.service.HallService;
import com.ftninformatika.jwd.modul3.cinema.support.HallDtoToHall;
import com.ftninformatika.jwd.modul3.cinema.support.HallToHallDto;
import com.ftninformatika.jwd.modul3.cinema.web.dto.HallDTO;

@RestController
@RequestMapping(value = "/api/halls", produces = MediaType.APPLICATION_JSON_VALUE)
public class HallController {
	
	@Autowired
	private HallService service;
	
	@Autowired
	private HallToHallDto toHallDto;
	
	@Autowired
	private HallDtoToHall toHall;

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<HallDTO> create (@RequestBody HallDTO dto) {
		
		Hall hall = toHall.convert(dto);
		Hall saved = service.save(hall);
		
		return new ResponseEntity<HallDTO>(toHallDto.convert(saved), HttpStatus.CREATED);
	}
	
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<HallDTO> edit (@PathVariable Long id, @RequestBody HallDTO dto) {
		
		if(!id.equals(dto.getId()))
			return new ResponseEntity<HallDTO>(HttpStatus.BAD_REQUEST);
		
		Hall hall = toHall.convert(dto);
		Hall saved = service.save(hall);
		
		return new ResponseEntity<HallDTO>(toHallDto.convert(saved), HttpStatus.CREATED);
	}
	
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Void> delete (@PathVariable Long id) {
		
		Hall deleted = service.delete(id);
		
		if(deleted != null) {
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}	
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<HallDTO> getOne(@PathVariable Long id){
	   Hall hall = service.findOne(id);
	
	    if(hall != null) {
	        return new ResponseEntity<>(toHallDto.convert(hall), HttpStatus.OK);
	    }else {
	        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    }
	}
	
	@GetMapping
	public ResponseEntity<List<HallDTO>> getAll(){
	
	    List<Hall> halls = service.findAll();
	
	    return new ResponseEntity<>(toHallDto.convert(halls), HttpStatus.OK);
	}
}
