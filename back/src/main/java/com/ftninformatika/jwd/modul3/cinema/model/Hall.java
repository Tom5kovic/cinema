package com.ftninformatika.jwd.modul3.cinema.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
public class Hall {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@Column(nullable = false)
    private String name;
	
	@OneToMany(mappedBy = "hall", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Seat> seats = new ArrayList<Seat>();
	
	@OneToMany(mappedBy = "hall", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Projection> projections = new ArrayList<Projection>();

	public Hall() {
		super();
	}

	public Hall(Long id, String name, List<Seat> seats, List<Projection> projections) {
		super();
		this.id = id;
		this.name = name;
		this.seats = seats;
		this.projections = projections;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Seat> getSeats() {
		return seats;
	}

	public void setSeats(List<Seat> seats) {
		this.seats = seats;
	}
	
	public List<Projection> getProjections() {
		return projections;
	}

	public void setProjections(List<Projection> projections) {
		this.projections = projections;
	}

	@Override
	public String toString() {
		return "Hall [id=" + id + ", name=" + name + ", seats=" + seats + ", projections=" + projections + "]";
	}

}
