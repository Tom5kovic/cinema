package com.ftninformatika.jwd.modul3.cinema.web.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class UserDTO {

	@Positive
    private Long id;

    @NotBlank
    private String username;

    @NotEmpty
    @Email
    private String eMail;

    @Size(min=3, max=50)
    private String firstName;

    @Size(min=3, max=50)
    private String lastName;

    private AdressDTO adressDTO;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public AdressDTO getAdressDTO() {
		return adressDTO;
	}

	public void setAdressDTO(AdressDTO adressDTO) {
		this.adressDTO = adressDTO;
	}

}
