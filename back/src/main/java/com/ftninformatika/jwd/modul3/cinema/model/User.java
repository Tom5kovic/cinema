package com.ftninformatika.jwd.modul3.cinema.model;

import com.ftninformatika.jwd.modul3.cinema.enumeration.UserRole;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.*;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, nullable = false)
    private String username;

    @Column( unique = true, nullable = false)
    private String eMail;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column(nullable = false)
    private String password;

    @Enumerated(EnumType.STRING)
    private UserRole userRole;

    @ManyToOne
    private Adress adress;
    
    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Ticket> tickets = new ArrayList<Ticket>();

    public User(){

    }

    public User(Long id, String username, String eMail, String firstName, String lastName, String password,
			UserRole userRole, Adress adress, List<Ticket> tickets) {
		super();
		this.id = id;
		this.username = username;
		this.eMail = eMail;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.userRole = userRole;
		this.adress = adress;
		this.tickets = tickets;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Ticket> getTickets() {
		return tickets;
	}

	public void setTickets(List<Ticket> tickets) {
		this.tickets = tickets;
	}

	public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public Adress getAdress() {
        return adress;
    }

    public void setAdress(Adress adress) {
        this.adress = adress;
        if (adress != null && !adress.getUsers().contains(this)) {
           adress.getUsers().add(this);
        }
    }
    
//    public void buyTicket(Ticket ticket) {
//    	Optional<User> userOptional = Optional.ofNullable(this);
//    	this.tickets.add(ticket);
//    	if (ticket != null && !equals(ticket.getUser())) {
//    		ticket.setUser(userOptional);
//    	}
//    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        User other = (User) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "User [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + "]";
    }

}
