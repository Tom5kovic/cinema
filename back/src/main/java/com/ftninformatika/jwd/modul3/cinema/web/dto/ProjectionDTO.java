package com.ftninformatika.jwd.modul3.cinema.web.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;

public class ProjectionDTO {

    @Positive(message = "Id must be positive number.")
    private Long id;

    @NotBlank(message = "Projection must have date and time.")
    @Pattern(regexp = "^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) (2[0-3]|[01][0-9]):[0-5][0-9]$", message = "Date or time is not valid.")
    private String dateTime;

    @NotNull(message = "Projection must have movie.")
    private MovieDTO movie;

    @NotNull(message = "Projection must have hall.")
    private HallDTO hall;

    @NotBlank(message = "Projection must have type.")
    private String type;

    @NotNull(message = "Projection must have ticket price.")
    @Positive(message = "Ticket price is not positive number.")
    private double ticketPrice;

    public ProjectionDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public MovieDTO getMovie() {
		return movie;
	}

	public void setMovie(MovieDTO movie) {
		this.movie = movie;
	}
	
	public HallDTO getHall() {
		return hall;
	}

	public void setHall(HallDTO hall) {
		this.hall = hall;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getTicketPrice() {
		return ticketPrice;
	}

	public void setTicketPrice(double ticketPrice) {
		this.ticketPrice = ticketPrice;
	}

  
}
