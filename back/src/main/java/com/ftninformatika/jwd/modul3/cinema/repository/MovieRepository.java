package com.ftninformatika.jwd.modul3.cinema.repository;

import com.ftninformatika.jwd.modul3.cinema.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MovieRepository extends JpaRepository<Movie,Long> {

    Movie findOneById(Long id);

    List<Movie> findByGenresId(Long genreId);

    List<Movie> findByNameIgnoreCaseContainsAndDurationBetweenAndGenresId(String name,Integer durationFrom,Integer durationTo,Long genreId);

    List<Movie> findByNameIgnoreCaseContainsAndDurationBetween(String name,Integer durationFrom,Integer durationTo);

    /*
     * Pronalazi sve objekte tipa Film i vraca onoliko objekata koliko je
     * specificirano kroz Pageable objekat. Npr. ako se prosledi objekat: new
     * PageRequest(0, 10) vratice se nulta stranica sa prvih 10 objekata tipa Film.
     */
//    Page<Film> findAll(Pageable pageable);

}

