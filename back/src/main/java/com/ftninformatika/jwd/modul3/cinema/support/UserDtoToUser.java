package com.ftninformatika.jwd.modul3.cinema.support;

import com.ftninformatika.jwd.modul3.cinema.model.User;
import com.ftninformatika.jwd.modul3.cinema.service.AdressService;
import com.ftninformatika.jwd.modul3.cinema.service.UserService;
import com.ftninformatika.jwd.modul3.cinema.web.dto.UserDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

@Component
public class UserDtoToUser implements Converter<UserDTO, User> {

    @Autowired
    private UserService userService;

    @Autowired
    private AdressService adressService;

    @Override
    public User convert(UserDTO dto) {
        User user = null;
        if(dto.getId() != null) {
            user = userService.findOne(dto.getId()).get();
        }

        if(user == null) {
            user = new User();
        }

        user.setUsername(dto.getUsername());
        user.setAdress(adressService.findOne(dto.getAdressDTO().getId()).get());
        user.seteMail(dto.geteMail());
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());

        return user;
    }

}
