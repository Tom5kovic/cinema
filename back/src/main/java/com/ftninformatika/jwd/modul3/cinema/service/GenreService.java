package com.ftninformatika.jwd.modul3.cinema.service;

import com.ftninformatika.jwd.modul3.cinema.model.Genre;

import java.util.List;

public interface GenreService {

    Genre findOne(Long id);

    List<Genre> findAll();

    List<Genre> find(List<Long> ids);

    Genre save(Genre genre);

    Genre update(Genre genre);

    Genre delete(Long id);

    List<Genre> find(String name);
}
