package com.ftninformatika.jwd.modul3.cinema.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Projection {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="date_time", nullable = false)
    private LocalDateTime dateTime;

    @ManyToOne
    private Movie movie;

    @ManyToOne
    private Hall hall;

    @Column
    private String type;

    @Column
    private double ticketPrice;
    
    @OneToMany(mappedBy = "projection", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Ticket> tickets = new ArrayList<Ticket>();

    public Projection() {
        super();
    }

    public Projection(Long id, LocalDateTime dateTime, Movie movie, Hall hall, String type, double ticketPrice) {
		super();
		this.id = id;
		this.dateTime = dateTime;
		this.movie = movie;
		this.hall = hall;
		this.type = type;
		this.ticketPrice = ticketPrice;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Ticket> getTickets() {
		return tickets;
	}

	public void setTickets(List<Ticket> tickets) {
		this.tickets = tickets;
	}

	public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public Movie getMovie() {
        return movie;
    }
    
    public void addTicket(Ticket ticket) {
    	this.tickets.add(ticket);
    	if(!equals(ticket.getProjection())){
            ticket.setProjection(this);
        }
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
        if(movie !=null && !movie.getProjection().contains(this)){
            movie.getProjection().add(this);
        }
    }

    public Hall getHall() {
		return hall;
	}

	public void setHall(Hall hall) {
		this.hall = hall;
	}

	public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(double ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Projection other = (Projection) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        String movieName = movie == null ? " - " : movie.getName();
        return "Projection [id=" + id + ", movie=" + movieName +", time=" + dateTime+ ", type=" + type +
                ", hall=" + hall + ", ticket price=" + ticketPrice + "]";
    }

}
