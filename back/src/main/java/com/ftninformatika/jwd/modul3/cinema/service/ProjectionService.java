package com.ftninformatika.jwd.modul3.cinema.service;

import com.ftninformatika.jwd.modul3.cinema.model.Projection;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.domain.Page;

public interface ProjectionService {

    Projection findOne(Long id);

    List<Projection> findAll();

    Projection save(Projection projection);

    Projection update(Projection projection);

    Projection delete(Long id);

//    List<Projection> find(LocalDateTime dateTimeFrom, LocalDateTime dateTimeTo, 
//    					Long movieId, String type, Integer hall, Double ticketPriceFrom, Double ticketPriceTo);
    
    List<Projection> findByMovieId(Long movieId);
    
    Page<Projection> search (Long movieId, LocalDateTime dateTimeFrom, String type, int pageNum);
}
