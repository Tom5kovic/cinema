package com.ftninformatika.jwd.modul3.cinema.web.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.modul3.cinema.model.Adress;
import com.ftninformatika.jwd.modul3.cinema.model.Projection;
import com.ftninformatika.jwd.modul3.cinema.model.Ticket;
import com.ftninformatika.jwd.modul3.cinema.service.TicketService;
import com.ftninformatika.jwd.modul3.cinema.support.TicketDtoToTicket;
import com.ftninformatika.jwd.modul3.cinema.support.TicketToTicketDto;
import com.ftninformatika.jwd.modul3.cinema.web.dto.AdressDTO;
import com.ftninformatika.jwd.modul3.cinema.web.dto.ProjectionDTO;
import com.ftninformatika.jwd.modul3.cinema.web.dto.TicketDTO;

@RestController
@RequestMapping(value = "/api/tickets", produces = MediaType.APPLICATION_JSON_VALUE)
public class TicketController {

	@Autowired
	private TicketService service;
	
	@Autowired
	private TicketToTicketDto toDto;
	
	@Autowired
	private TicketDtoToTicket toTicket;
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TicketDTO> create(@Valid @RequestBody TicketDTO dto){
        Ticket ticket = toTicket.convert(dto);

        if(ticket.getUser() == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        
        Ticket saved = service.save(ticket);

        return new ResponseEntity<>(toDto.convert(saved), HttpStatus.CREATED);
    }
	
	@PutMapping(value= "/{id}",consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TicketDTO> update(@PathVariable Long id, @Valid @RequestBody TicketDTO dto){

        if(!id.equals(dto.getId())) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Ticket ticket = toTicket.convert(dto);

        if(ticket.getUser() == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Ticket saved = service.update(ticket);

        return new ResponseEntity<>(toDto.convert(saved),HttpStatus.OK);
    }
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id){
	
	    Optional<Ticket> ticket = service.findOne(id);
	    if(!ticket.isPresent()) {
	        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    }
	    
	    if(ticket.get().getUser() != null){
	        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	    }
	
	    service.delete(id);
	    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<TicketDTO> getOne(@PathVariable Long id){
	    Ticket ticket = service.findOne(id).get();
	
	    if(ticket != null) {
	        return new ResponseEntity<>(toDto.convert(ticket), HttpStatus.OK);
	    }else {
	        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    }
	}

	@GetMapping
    public ResponseEntity<List<TicketDTO>> getAll(){

        List<Ticket> tickets = service.findAll();

        return new ResponseEntity<>(toDto.convert(tickets), HttpStatus.OK);
    }
}
