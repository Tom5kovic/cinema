package com.ftninformatika.jwd.modul3.cinema.service;

import com.ftninformatika.jwd.modul3.cinema.model.Movie;

import java.util.List;

public interface MovieService {

    Movie findOne(Long id);

    List<Movie> findAll();

    Movie save(Movie movie);

    Movie update(Movie movie);

    Movie delete(Long id);

    List<Movie> find(String name, Long genreId, Integer durationFrom, Integer durationTo);

    List<Movie> findByGenreId(Long genreId);
}
