package com.ftninformatika.jwd.modul3.cinema.support;

import com.ftninformatika.jwd.modul3.cinema.model.Movie;
import com.ftninformatika.jwd.modul3.cinema.model.Genre;
import com.ftninformatika.jwd.modul3.cinema.web.dto.MovieDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Component
public class MovieToMovieDto implements Converter<Movie, MovieDTO> {

    @Autowired
    private GenreToGenreDto toGenreDto;

    @Override
    public MovieDTO convert(Movie movie) {
        MovieDTO dto = new MovieDTO();
        dto.setId(movie.getId());
        dto.setName(movie.getName());
        dto.setDuration(movie.getDuration());
        List<Genre> genres = new ArrayList<>(movie.getGenres());
        dto.setGenres(new HashSet<>(toGenreDto.convert(genres)));
        return dto;
    }

    public List<MovieDTO> convert(List<Movie> movies){
        List<MovieDTO> moviesDto = new ArrayList<>();

        for(Movie movie : movies) {
            moviesDto.add(convert(movie));
        }

        return moviesDto;
    }

}

