package com.ftninformatika.jwd.modul3.cinema.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul3.cinema.model.Seat;
import com.ftninformatika.jwd.modul3.cinema.service.HallService;
import com.ftninformatika.jwd.modul3.cinema.service.SeatService;
import com.ftninformatika.jwd.modul3.cinema.web.dto.SeatDTO;

@Component
public class SeatDtoToSeat implements Converter<SeatDTO, Seat> {

	@Autowired
	private SeatService seatService;
	
	@Autowired
	private HallService hallService;
	
	@Override
	public Seat convert(SeatDTO dto) {
		Seat entity;
		
		if (dto.getId() == null) {
			entity = new Seat();
		} else {
			entity = seatService.findOne(dto.getId());
		}
		
		if (entity != null) {
			entity.setHall(hallService.findOne(dto.getHall().getId()));
		}
		
		return entity;
	}

}
