package com.ftninformatika.jwd.modul3.cinema.enumeration;

public enum UserRole {
    ADMIN,
    USER
}
