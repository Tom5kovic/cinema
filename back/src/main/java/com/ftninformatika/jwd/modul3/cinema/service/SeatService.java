package com.ftninformatika.jwd.modul3.cinema.service;

import java.util.List;
import com.ftninformatika.jwd.modul3.cinema.model.Seat;

public interface SeatService {

	Seat findOne (Long id);
	
	List<Seat> findAll();

	Seat save(Seat seat);

	Seat delete(Long id);
}
