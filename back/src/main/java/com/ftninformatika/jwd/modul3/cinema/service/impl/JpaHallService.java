package com.ftninformatika.jwd.modul3.cinema.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.modul3.cinema.model.Hall;
import com.ftninformatika.jwd.modul3.cinema.model.Movie;
import com.ftninformatika.jwd.modul3.cinema.repository.HallRepository;
import com.ftninformatika.jwd.modul3.cinema.service.HallService;

@Service
public class JpaHallService implements HallService {

	@Autowired
	private HallRepository repository;
	
	@Override
	public Hall findOne(Long id) {
		return repository.findOneById(id);
	}

	@Override
	public List<Hall> findAll() {
		return repository.findAll();
	}

	@Override
	public Hall save(Hall hall) {
		return repository.save(hall);
	}

	@Override
	public Hall delete(Long id) {
		Optional<Hall> hall = repository.findById(id);
        if(hall.isPresent()){
            repository.deleteById(id);
            return hall.get();
        }
        return null;
	}
}
