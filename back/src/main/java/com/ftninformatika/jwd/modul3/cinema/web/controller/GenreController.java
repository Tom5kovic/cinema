package com.ftninformatika.jwd.modul3.cinema.web.controller;

import com.ftninformatika.jwd.modul3.cinema.model.Movie;
import com.ftninformatika.jwd.modul3.cinema.model.Genre;
import com.ftninformatika.jwd.modul3.cinema.service.MovieService;
import com.ftninformatika.jwd.modul3.cinema.service.GenreService;
import com.ftninformatika.jwd.modul3.cinema.support.MovieToMovieDto;
import com.ftninformatika.jwd.modul3.cinema.support.GenreDtoToGenre;
import com.ftninformatika.jwd.modul3.cinema.support.GenreToGenreDto;
import com.ftninformatika.jwd.modul3.cinema.web.dto.MovieDTO;
import com.ftninformatika.jwd.modul3.cinema.web.dto.GenreDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api/genres", produces = MediaType.APPLICATION_JSON_VALUE)
public class GenreController {

    @Autowired
    private GenreService genreService;

    @Autowired
    private MovieService movieService;

    @Autowired
    private GenreToGenreDto toGenreDto;

    @Autowired
    private GenreDtoToGenre toGenre;

    @Autowired
    private MovieToMovieDto toMovieDto;

    //@PreAuthorize("hasRole('ADMIN')")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenreDTO> create(@Valid @RequestBody GenreDTO dto){
        Genre genre = toGenre.convert(dto);
        Genre saved = genreService.save(genre);

        return new ResponseEntity<>(toGenreDto.convert(saved), HttpStatus.CREATED);
    }

    //@PreAuthorize("hasRole('ADMIN')")
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenreDTO> update(@PathVariable Long id, @Valid @RequestBody GenreDTO dto){

        if(!id.equals(dto.getId())) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Genre genre = toGenre.convert(dto);
        Genre saved = genreService.update(genre);

        return new ResponseEntity<>(toGenreDto.convert(saved),HttpStatus.OK);
    }

    //@PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id){
        Genre deleted = genreService.delete(id);

        if(deleted != null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
    @GetMapping("/{id}")
    public ResponseEntity<GenreDTO> getOne(@PathVariable Long id){
        Genre genre = genreService.findOne(id);

        if(genre != null) {
            return new ResponseEntity<>(toGenreDto.convert(genre), HttpStatus.OK);
        }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
    @GetMapping
    public ResponseEntity<List<GenreDTO>> getAll(
            @RequestParam(required=false) String name){

        List<Genre> genres;

        if(name == null){
            genres = genreService.findAll();
        }else {
            genres = genreService.find(name);
        }

        return new ResponseEntity<>(toGenreDto.convert(genres), HttpStatus.OK);
    }

    //@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
    @GetMapping("/{id}/filmovi")
    public ResponseEntity<List<MovieDTO>> findByGenreId(@PathVariable Long id){
        List<Movie> movies = movieService.findByGenreId(id);

        return new ResponseEntity<>(toMovieDto.convert(movies), HttpStatus.OK);
    }
}
