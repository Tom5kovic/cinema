package com.ftninformatika.jwd.modul3.cinema.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul3.cinema.model.Hall;
import com.ftninformatika.jwd.modul3.cinema.web.dto.HallDTO;

@Component
public class HallToHallDto implements Converter<Hall, HallDTO> {

	@Override
	public HallDTO convert(Hall hall) {
		HallDTO dto = new HallDTO();
		dto.setId(hall.getId());
		dto.setName(hall.getName());
		return dto;
	}
	
	public List<HallDTO> convert (List<Hall> halls) {
		List<HallDTO> hallDtos = new ArrayList<HallDTO>();
		
		for (Hall hall : halls) {
			hallDtos.add(convert(hall));
		}
		
		return hallDtos;
	}

}
