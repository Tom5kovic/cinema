package com.ftninformatika.jwd.modul3.cinema.repository;

import com.ftninformatika.jwd.modul3.cinema.model.Genre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GenreRepository extends JpaRepository<Genre,Long> {

    Genre findOneById(Long id);

    List<Genre> findByNameIgnoreCaseContains(String name);

    List<Genre> findByIdIn(List<Long> ids);
}
