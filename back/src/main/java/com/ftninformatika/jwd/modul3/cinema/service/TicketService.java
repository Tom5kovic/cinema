package com.ftninformatika.jwd.modul3.cinema.service;

import java.util.List;
import java.util.Optional;

import com.ftninformatika.jwd.modul3.cinema.model.Projection;
import com.ftninformatika.jwd.modul3.cinema.model.Ticket;

public interface TicketService {

	Optional<Ticket> findOne (Long id);
	
	List<Ticket> findAll();

	Ticket save(Ticket ticket);

	Ticket update(Ticket ticket);

	void delete(Long id);
	
	List<Ticket> findByUserId(Long userId);
}
