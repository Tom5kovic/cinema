package com.ftninformatika.jwd.modul3.cinema.support;
import com.ftninformatika.jwd.modul3.cinema.model.User;
import com.ftninformatika.jwd.modul3.cinema.web.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserToUserDto implements Converter<User, UserDTO>{

    @Autowired
    private AdressToAdressDto toAdressDto;

    @Override
    public UserDTO convert(User user) {
        UserDTO dto = new UserDTO();

        dto.setId(user.getId());
        dto.setAdressDTO(toAdressDto.convert(user.getAdress()));
        dto.seteMail(user.geteMail());
        dto.setFirstName(user.getFirstName());
        dto.setLastName(user.getLastName());
        dto.setUsername(user.getUsername());

        return dto;
    }

    public List<UserDTO> convert(List<User> users){
        List<UserDTO> usersDto = new ArrayList<>();

        for(User u : users) {
            UserDTO dto = convert(u);
            usersDto.add(dto);
        }

        return usersDto;
    }
}
