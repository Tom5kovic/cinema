package com.ftninformatika.jwd.modul3.cinema.repository;

import com.ftninformatika.jwd.modul3.cinema.model.Projection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface ProjectionRepository extends JpaRepository<Projection,Long> {

    Projection findOneById(Long id);

//    List<Projection> findByDatumIVremeBetweenAndCenaKarteBetweenAndTipLike(LocalDateTime datumIVremeOd,LocalDateTime datumIVremeDo,
//                                                                           Double cenaKarteOd,Double cenaKarteDo,String tip);
//
//    List<Projection> findByDatumIVremeBetweenAndCenaKarteBetweenAndTipLikeAndFilmId(LocalDateTime datumIVremeOd,LocalDateTime datumIVremeDo,
//                                                                           Double cenaKarteOd,Double cenaKarteDo,String tip,Long filmId);
//
//    List<Projection> findByDatumIVremeBetweenAndCenaKarteBetweenAndTipLikeAndSala(LocalDateTime datumIVremeOd,LocalDateTime datumIVremeDo,
//                                                                                    Double cenaKarteOd,Double cenaKarteDo,String tip,Integer sala);
//
//    List<Projection> findByDatumIVremeBetweenAndCenaKarteBetweenAndTipLikeAndFilmIdAndSala(LocalDateTime datumIVremeOd,LocalDateTime datumIVremeDo,
//                                                                                    Double cenaKarteOd,Double cenaKarteDo,String tip,Long filmId,Integer sala);
    List<Projection> findByMovieId(Long movieId);
    
    @Query("SELECT p FROM Projection p WHERE "
    		+ "(:movieId IS NULL OR p.movie.id = :movieId) AND "
    		+ "(:dateTimeFrom IS NULL OR p.dateTime >= :dateTimeFrom) AND "
    		+ "(:type IS NULL OR p.type like %:type% )")
    Page<Projection> search(@Param("movieId") Long movieId, @Param("dateTimeFrom") LocalDateTime dateTimeFrom, @Param("type") String type, Pageable pageable);
}
