package com.ftninformatika.jwd.modul3.cinema.support;

import com.ftninformatika.jwd.modul3.cinema.model.Projection;
import com.ftninformatika.jwd.modul3.cinema.service.HallService;
import com.ftninformatika.jwd.modul3.cinema.service.MovieService;
import com.ftninformatika.jwd.modul3.cinema.service.ProjectionService;
import com.ftninformatika.jwd.modul3.cinema.web.dto.ProjectionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.core.convert.converter.Converter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

@Component
public class ProjectionDtoToProjection implements Converter<ProjectionDTO, Projection> {

    @Autowired
    private ProjectionService projectionService;

    @Autowired
    private MovieService movieService;
    
    @Autowired
    private HallService hallService;

    @Override
    public Projection convert(ProjectionDTO dto) {
        Projection projection;

        if(dto.getId() == null){
            projection = new Projection();
        }else{
            projection = projectionService.findOne(dto.getId());
        }

        if(projection != null){
            projection.setDateTime(getLocalDateTime(dto.getDateTime()));
            projection.setMovie(movieService.findOne(dto.getMovie().getId()));
            projection.setHall(hallService.findOne(dto.getHall().getId()));
            projection.setType(dto.getType());
            projection.setTicketPrice(dto.getTicketPrice());
        }
        return projection;
    }

    private LocalDateTime getLocalDateTime(String dateTime) throws DateTimeParseException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate date = LocalDate.parse(dateTime.substring(0, 10), formatter);
        LocalTime time = LocalTime.parse(dateTime.substring(11), DateTimeFormatter.ofPattern("HH:mm"));
        return LocalDateTime.of(date, time);
    }
}
