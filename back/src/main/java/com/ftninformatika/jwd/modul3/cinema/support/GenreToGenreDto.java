package com.ftninformatika.jwd.modul3.cinema.support;
import com.ftninformatika.jwd.modul3.cinema.model.Genre;
import com.ftninformatika.jwd.modul3.cinema.web.dto.GenreDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class GenreToGenreDto implements Converter<Genre, GenreDTO> {

    @Override
    public GenreDTO convert(Genre genre) {
        GenreDTO dto = new GenreDTO();
        dto.setId(genre.getId());
        dto.setName(genre.getName());
        return dto;
    }

    public List<GenreDTO> convert(List<Genre> genres){
        List<GenreDTO> genresDto = new ArrayList<>();

        for(Genre zanr : genres) {
            genresDto.add(convert(zanr));
        }

        return genresDto;
    }
}
