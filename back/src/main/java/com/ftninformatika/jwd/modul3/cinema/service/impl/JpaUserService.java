package com.ftninformatika.jwd.modul3.cinema.service.impl;

import com.ftninformatika.jwd.modul3.cinema.enumeration.UserRole;
import com.ftninformatika.jwd.modul3.cinema.model.User;
import com.ftninformatika.jwd.modul3.cinema.repository.UserRepository;
import com.ftninformatika.jwd.modul3.cinema.service.UserService;
import com.ftninformatika.jwd.modul3.cinema.web.dto.UserChangePasswordDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class JpaUserService implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Optional<User> findOne(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public Page<User> findAll(int pageNumber) {
        return userRepository.findAll(PageRequest.of(pageNumber,10));
    }

    @Override
    public User save(User user) {
        user.setUserRole(UserRole.USER);
        return userRepository.save(user);
    }

    @Override
    public void delete(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public boolean changePassword(Long id, UserChangePasswordDto changePasswordDto) {
        Optional<User> result = userRepository.findById(id);

        if(!result.isPresent()) {
            throw new EntityNotFoundException();
        }

        User user = result.get();

        if(!user.getUsername().equals(changePasswordDto.getUsername())
                || !user.getPassword().equals(changePasswordDto.getPassword())){
            return false;
        }

        String password = changePasswordDto.getPassword();
        if (!changePasswordDto.getPassword().equals("")) {
            password = passwordEncoder.encode(changePasswordDto.getPassword());
        }

        user.setPassword(password);

        userRepository.save(user);

        return true;
    }

	@Override
	public Optional<User> findbyUsername(String username) {
		return userRepository.findFirstByUsername(username);
	}
}
