package com.ftninformatika.jwd.modul3.cinema.support;

import com.ftninformatika.jwd.modul3.cinema.model.Movie;
import com.ftninformatika.jwd.modul3.cinema.model.Genre;
import com.ftninformatika.jwd.modul3.cinema.service.MovieService;
import com.ftninformatika.jwd.modul3.cinema.service.GenreService;
import com.ftninformatika.jwd.modul3.cinema.web.dto.MovieDTO;
import com.ftninformatika.jwd.modul3.cinema.web.dto.GenreDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class MovieDtoToMovie implements Converter<MovieDTO,Movie> {

    @Autowired
    private MovieService movieService;

    @Autowired
    private GenreService genreService;

    @Override
    public Movie convert(MovieDTO dto) {

        Movie entity;

        if(dto.getId() == null) {
            entity = new Movie();
        }else {
            entity = movieService.findOne(dto.getId());
        }

        if(entity != null) {
            entity.setName(dto.getName());
            entity.setDuration(dto.getDuration());
            
            if(dto.getGenres() != null) {
            	List<Long> genresIds = dto.getGenres().stream().map(GenreDTO::getId).collect(Collectors.toList());
                List<Genre> genres = genreService.find(genresIds);
                entity.setGenres(new HashSet<>(genres));
            }
        }

        return entity;
    }
}
