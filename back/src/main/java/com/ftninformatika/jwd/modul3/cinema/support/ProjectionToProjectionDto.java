package com.ftninformatika.jwd.modul3.cinema.support;

import com.ftninformatika.jwd.modul3.cinema.model.Projection;
import com.ftninformatika.jwd.modul3.cinema.web.dto.ProjectionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProjectionToProjectionDto implements Converter<Projection, ProjectionDTO> {

    @Autowired
    private MovieToMovieDto toMovieDto;
    
    @Autowired
    private HallToHallDto toHallDto;

    @Override
    public ProjectionDTO convert(Projection projection) {
        ProjectionDTO dto = new ProjectionDTO();
        dto.setId(projection.getId());
        dto.setDateTime(projection.getDateTime().toString());
        dto.setMovie(toMovieDto.convert(projection.getMovie()));
        dto.setHall(toHallDto.convert(projection.getHall()));
        dto.setType(projection.getType());
        dto.setTicketPrice(projection.getTicketPrice());

        return dto;
    }

    public List<ProjectionDTO> convert(List<Projection> projection){
        List<ProjectionDTO> projectionsDto = new ArrayList<>();

        for(Projection p : projection) {
            projectionsDto.add(convert(p));
        }

        return projectionsDto;
    }
}
