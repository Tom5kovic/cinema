package com.ftninformatika.jwd.modul3.cinema.web.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class SeatDTO {

	@Positive(message = "Number need to be positive.")
	private Long id;
	
	@NotNull(message = "Seat must have hall.")
	private HallDTO hall;
	
	private TicketDTO ticket;

	public SeatDTO() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TicketDTO getTicket() {
		return ticket;
	}

	public void setTicket(TicketDTO ticket) {
		this.ticket = ticket;
	}

	public HallDTO getHall() {
		return hall;
	}

	public void setHall(HallDTO hall) {
		this.hall = hall;
	}

}
