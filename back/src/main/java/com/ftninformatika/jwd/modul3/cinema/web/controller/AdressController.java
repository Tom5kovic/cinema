package com.ftninformatika.jwd.modul3.cinema.web.controller;

import com.ftninformatika.jwd.modul3.cinema.model.Adress;
import com.ftninformatika.jwd.modul3.cinema.service.AdressService;
import com.ftninformatika.jwd.modul3.cinema.support.AdressDtoToAdress;
import com.ftninformatika.jwd.modul3.cinema.support.AdressToAdressDto;
import com.ftninformatika.jwd.modul3.cinema.web.dto.AdressDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/adresses", produces = MediaType.APPLICATION_JSON_VALUE)
public class AdressController {

    @Autowired
    private AdressService service;

    @Autowired
    private AdressDtoToAdress toAdress;

    @Autowired
    private AdressToAdressDto toAdressDto;

    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AdressDTO> create(@RequestBody AdressDTO dto){

        Adress adress = toAdress.convert(dto);
        Adress saved = service.save(adress);

        return new ResponseEntity<>(toAdressDto.convert(saved), HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @PutMapping(value= "/{id}",consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AdressDTO> edit( @PathVariable Long id, @RequestBody AdressDTO dto){

        if(!id.equals(dto.getId())) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Adress adress = toAdress.convert(dto);
        Adress saved = service.save(adress);

        return new ResponseEntity<>(toAdressDto.convert(saved), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id){

        Optional<Adress> adress = service.findOne(id);
        if(!adress.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        if(!adress.get().getUsers().isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        service.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @GetMapping("/{id}")
    public ResponseEntity<AdressDTO> getOne(@PathVariable Long id){
        Adress adress = service.findOne(id).get();

        if(adress != null) {
            return new ResponseEntity<>(toAdressDto.convert(adress), HttpStatus.OK);
        }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping
    public ResponseEntity<List<AdressDTO>> getAll(){

        List<Adress> adresses = service.findAll();

        return new ResponseEntity<>(toAdressDto.convert(adresses), HttpStatus.OK);
    }
}
