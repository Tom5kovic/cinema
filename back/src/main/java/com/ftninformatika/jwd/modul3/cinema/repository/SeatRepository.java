package com.ftninformatika.jwd.modul3.cinema.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ftninformatika.jwd.modul3.cinema.model.Seat;

@Repository
public interface SeatRepository extends JpaRepository<Seat, Long> {

	Seat findOneById (Long id);
}
