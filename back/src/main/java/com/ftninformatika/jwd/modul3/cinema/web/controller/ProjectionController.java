package com.ftninformatika.jwd.modul3.cinema.web.controller;

import com.ftninformatika.jwd.modul3.cinema.model.Projection;
import com.ftninformatika.jwd.modul3.cinema.service.ProjectionService;
import com.ftninformatika.jwd.modul3.cinema.support.ProjectionDtoToProjection;
import com.ftninformatika.jwd.modul3.cinema.support.ProjectionToProjectionDto;
import com.ftninformatika.jwd.modul3.cinema.web.dto.ProjectionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

@RestController
@RequestMapping(value = "/api/projections", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProjectionController {

    @Autowired
    private ProjectionService service;

    @Autowired
    private ProjectionDtoToProjection toProjeection;

    @Autowired
    private ProjectionToProjectionDto toProjectionDto;

    //@PreAuthorize("hasRole('ADMIN')")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProjectionDTO> create(@Valid @RequestBody ProjectionDTO dto){
        Projection projection = toProjeection.convert(dto);

        if(projection.getMovie() == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        
        Projection saved = service.save(projection);

        return new ResponseEntity<>(toProjectionDto.convert(saved), HttpStatus.CREATED);
    }

    //@PreAuthorize("hasRole('ADMIN')")
    @PutMapping(value= "/{id}",consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProjectionDTO> update(@PathVariable Long id, @Valid @RequestBody ProjectionDTO dto){

        if(!id.equals(dto.getId())) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Projection projection = toProjeection.convert(dto);

        if(projection.getMovie() == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Projection saved = service.update(projection);

        return new ResponseEntity<>(toProjectionDto.convert(saved),HttpStatus.OK);
    }

    //@PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id){
        Projection deleted = service.delete(id);

        if(deleted != null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
    @GetMapping("/{id}")
    public ResponseEntity<ProjectionDTO> getOne(@PathVariable Long id){
        Projection projection = service.findOne(id);

        if(projection != null) {
            return new ResponseEntity<>(toProjectionDto.convert(projection), HttpStatus.OK);
        }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    
    //@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
    @GetMapping
    public ResponseEntity<List<ProjectionDTO>> getAll(
    		@RequestParam(required=false) String dateTimeFromParameter,
//            @RequestParam(required=false) String datumIVremeDoParametar,
            @RequestParam(required=false) Long movieId,
            @RequestParam(required=false) String type,
//            @RequestParam(required=false) Integer sala,
//            @RequestParam(required=false) Double cenaKarteOd,
//            @RequestParam(required=false) Double cenaKarteDo,
            @RequestParam(defaultValue = "0") int pageNum){

        
        LocalDateTime dateTimeFrom = null;
        if(dateTimeFromParameter != null) {
        	dateTimeFrom = getLocalDateTime(dateTimeFromParameter);
        }
        
        Page<Projection> page = service.search(movieId, dateTimeFrom, type, pageNum);
        
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Total-Pages", page.getTotalPages() + "");
        
        return new ResponseEntity<>(toProjectionDto.convert(page.getContent()), responseHeaders, HttpStatus.OK);
    }

    private LocalDateTime getLocalDateTime(String dateTime) throws DateTimeParseException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate date = LocalDate.parse(dateTime.substring(0, 10), formatter);
        LocalTime time = LocalTime.parse(dateTime.substring(11), DateTimeFormatter.ofPattern("HH:mm"));
        return LocalDateTime.of(date, time);
    }
}
