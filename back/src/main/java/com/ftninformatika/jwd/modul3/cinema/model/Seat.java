package com.ftninformatika.jwd.modul3.cinema.model;

import javax.persistence.*;

@Entity
public class Seat {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; //number of seat
	
	@ManyToOne
	private Hall hall;
	
	@OneToOne
//	@JoinColumn(name = "ticket_id")
	private Ticket ticket;

	public Seat() {
		super();
	}

	public Seat(Long id, Hall hall) {
		super();
		this.id = id;
		this.hall = hall;
	}

	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Hall getHall() {
		return hall;
	}

	public void setHall(Hall hall) {
		this.hall = hall;
	}

	@Override
	public String toString() {
		return "Seat [id=" + id + ", hall=" + hall + "]";
	}
	
	
}
