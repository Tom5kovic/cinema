package com.ftninformatika.jwd.modul3.cinema.support;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul3.cinema.model.Ticket;
import com.ftninformatika.jwd.modul3.cinema.model.User;
import com.ftninformatika.jwd.modul3.cinema.service.ProjectionService;
import com.ftninformatika.jwd.modul3.cinema.service.SeatService;
import com.ftninformatika.jwd.modul3.cinema.service.TicketService;
import com.ftninformatika.jwd.modul3.cinema.service.UserService;
import com.ftninformatika.jwd.modul3.cinema.web.dto.TicketDTO;

@Component
public class TicketDtoToTicket implements Converter<TicketDTO, Ticket> {

	@Autowired
	private TicketService ticketService;
	
	@Autowired
	private ProjectionService projectionService;
	
	@Autowired 
	private SeatService seatService;
	
	@Autowired
	private UserService userService;
	
	@Override
	public Ticket convert(TicketDTO dto) {
		Ticket ticket = null;

        if(dto.getId() != null){
            ticket = ticketService.findOne(dto.getId()).get();
        }
        
        if (ticket == null) {
        	ticket = new Ticket();
        }
        
        if(ticket != null){
        	ticket.setDateTime(getLocalDateTime(dto.getDateTime()));
        	ticket.setProjection(projectionService.findOne(dto.getProjection().getId()));
        	ticket.setSeat(seatService.findOne(dto.getSeat().getId()));
        	ticket.setUser(userService.findOne(dto.getUser().getId()).get());
        }
        return ticket;
	}
	
	private LocalDateTime getLocalDateTime(String dateTime) throws DateTimeParseException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate date = LocalDate.parse(dateTime.substring(0, 10), formatter);
        LocalTime time = LocalTime.parse(dateTime.substring(11), DateTimeFormatter.ofPattern("HH:mm"));
        return LocalDateTime.of(date, time);
    }

}
