package com.ftninformatika.jwd.modul3.cinema.service.impl;

import com.ftninformatika.jwd.modul3.cinema.model.Genre;
import com.ftninformatika.jwd.modul3.cinema.repository.GenreRepository;
import com.ftninformatika.jwd.modul3.cinema.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class JpaGenreService implements GenreService {

    @Autowired
    private GenreRepository genreRepository;

    @Override
    public Genre findOne(Long id) {
        return genreRepository.findOneById(id);
    }

    @Override
    public List<Genre> findAll() {
        return genreRepository.findAll();
    }

    @Override
    public List<Genre> find(List<Long> ids) {
        return genreRepository.findByIdIn(ids);
    }

    @Override
    public Genre save(Genre genre) {
        return genreRepository.save(genre);
    }

    @Override
    public Genre update(Genre genre) {
        return genreRepository.save(genre);
    }

    @Override
    public Genre delete(Long id) {
        Optional<Genre> zanr = genreRepository.findById(id);
        if(zanr.isPresent()){
            genreRepository.deleteById(id);
            return zanr.get();
        }
        return null;
    }

    @Override
    public List<Genre> find(String name) {
        return genreRepository.findByNameIgnoreCaseContains(name);
    }
}
