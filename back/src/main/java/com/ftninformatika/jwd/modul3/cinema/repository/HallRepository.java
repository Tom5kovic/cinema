package com.ftninformatika.jwd.modul3.cinema.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ftninformatika.jwd.modul3.cinema.model.Hall;

@Repository
public interface HallRepository extends JpaRepository<Hall, Long> {

	Hall findOneById (Long id);
}
