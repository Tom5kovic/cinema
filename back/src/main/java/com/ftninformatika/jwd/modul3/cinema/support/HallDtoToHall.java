package com.ftninformatika.jwd.modul3.cinema.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul3.cinema.model.Hall;
import com.ftninformatika.jwd.modul3.cinema.service.HallService;
import com.ftninformatika.jwd.modul3.cinema.web.dto.HallDTO;

@Component
public class HallDtoToHall implements Converter<HallDTO, Hall> {

	@Autowired
	private HallService service;
	
	@Override
	public Hall convert(HallDTO dto) {
		Hall entity;
		
		if(dto.getId() == null) {
            entity = new Hall();
        }else {
            entity = service.findOne(dto.getId());
        }
		
		if (entity != null) {
			
			entity.setName(dto.getName());
		}
		
		return entity;
	}

	
}
