package com.ftninformatika.jwd.modul3.cinema.support;

import com.ftninformatika.jwd.modul3.cinema.model.Adress;
import com.ftninformatika.jwd.modul3.cinema.service.AdressService;
import com.ftninformatika.jwd.modul3.cinema.web.dto.AdressDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class AdressDtoToAdress implements Converter<AdressDTO, Adress> {

    @Autowired
    private AdressService service;

    @Override
    public Adress convert(AdressDTO adressDto) {
        Long id = adressDto.getId();
        Adress adress = id == null ? new Adress() : service.findOne(id).get();

        if(adress != null) {
            adress.setId(id);
            adress.setNumber(adressDto.getNumber());
            adress.setStreet(adressDto.getStreet());
        }

        return adress;
    }

}

