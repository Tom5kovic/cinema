package com.ftninformatika.jwd.modul3.cinema.service.impl;

import com.ftninformatika.jwd.modul3.cinema.model.Projection;
import com.ftninformatika.jwd.modul3.cinema.repository.ProjectionRepository;
import com.ftninformatika.jwd.modul3.cinema.service.ProjectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

@Service
public class JpaProjectionService implements ProjectionService {

    @Autowired
    private ProjectionRepository projectionRepository;

    @Override
    public Projection findOne(Long id) {
        return projectionRepository.findOneById(id);
    }

    @Override
    public List<Projection> findAll() {
        return projectionRepository.findAll();
    }

    @Override
    public Projection save(Projection projection) {
        return projectionRepository.save(projection);
    }

    @Override
    public Projection update(Projection projection) {
        return projectionRepository.save(projection);
    }

    @Override
    public Projection delete(Long id) {
        Projection projection = findOne(id);
        if(projection != null){
            projection.getMovie().getProjection().remove(projection);
            projection.setMovie(null);
            projection = projectionRepository.save(projection);
            projectionRepository.delete(projection);
            return projection;
        }
        return null;
    }

//    @Override
//    public List<Projection> find(LocalDateTime datumIVremeOd, LocalDateTime datumIVremeDo, Long filmId, String tip, Integer sala,
//                                 Double cenaKarteOd, Double cenaKarteDo) {
//        if (cenaKarteOd == null) {
//            cenaKarteOd = 0.0;
//        }
//
//        if (cenaKarteDo == null) {
//            cenaKarteDo = Double.MAX_VALUE;
//        }
//
//        if(filmId == null && sala == null){
//            return projectionRepository.findByDatumIVremeBetweenAndCenaKarteBetweenAndTipLike(datumIVremeOd,datumIVremeDo,cenaKarteOd,cenaKarteDo,tip);
//        }else if(filmId == null){
//            return projectionRepository.findByDatumIVremeBetweenAndCenaKarteBetweenAndTipLikeAndSala(datumIVremeOd,datumIVremeDo,cenaKarteOd,cenaKarteDo,tip,sala);
//        }else if(sala == null){
//            return projectionRepository.findByDatumIVremeBetweenAndCenaKarteBetweenAndTipLikeAndFilmId(datumIVremeOd,datumIVremeDo,cenaKarteOd,cenaKarteDo,tip,filmId);
//        }
//        return projectionRepository.findByDatumIVremeBetweenAndCenaKarteBetweenAndTipLikeAndFilmIdAndSala(datumIVremeOd,datumIVremeDo,cenaKarteOd,cenaKarteDo,tip,filmId,sala);
//    }
//
//    @Override
//    public List<Projection> findByFilmId(Long filmId) {
//        return projectionRepository.findByFilmId(filmId);
//    }
//
//    private LocalDateTime getDateConverted(String dateTime) throws DateTimeParseException {
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
//        return LocalDateTime.parse(dateTime, formatter);
//    }

	@Override
	public Page<Projection> search(Long movieId, LocalDateTime dateTimeFrom, String type, int pageNum) {
		
		return projectionRepository.search(movieId, dateTimeFrom, type, PageRequest.of(pageNum, 5));
	}

	@Override
	public List<Projection> findByMovieId(Long movieId) {
		return projectionRepository.findByMovieId(movieId);
	}

}
