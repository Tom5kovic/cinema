package com.ftninformatika.jwd.modul3.cinema.service;

import com.ftninformatika.jwd.modul3.cinema.model.User;
import com.ftninformatika.jwd.modul3.cinema.web.dto.UserChangePasswordDto;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

public interface UserService {

    Optional<User> findOne(Long id);

    List<User> findAll();

    Page<User> findAll(int pageNumber);

    User save(User user);

    void delete(Long id);

    Optional<User> findbyUsername(String username);

    boolean changePassword(Long id, UserChangePasswordDto changePasswordDto);
}
