package com.ftninformatika.jwd.modul3.cinema.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.modul3.cinema.model.Projection;
import com.ftninformatika.jwd.modul3.cinema.model.Ticket;
import com.ftninformatika.jwd.modul3.cinema.repository.TicketRepository;
import com.ftninformatika.jwd.modul3.cinema.service.TicketService;

@Service
public class JpaTicketService implements TicketService {

	@Autowired
	private TicketRepository repository;
	
	@Override
	public Optional<Ticket> findOne(Long id) {
		return repository.findById(id);
	}

	@Override
	public List<Ticket> findAll() {
		return repository.findAll();
	}

	@Override
	public Ticket save(Ticket ticket) {
		return repository.save(ticket);
	}

	@Override
	public Ticket update(Ticket ticket) {
		return repository.save(ticket);
	}

	@Override
	public void delete(Long id) {
		repository.deleteById(id);
	}

	@Override
	public List<Ticket> findByUserId(Long userId) {
		return repository.findByUserId(userId);
	}

}
