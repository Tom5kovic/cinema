package com.ftninformatika.jwd.modul3.cinema.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import com.ftninformatika.jwd.modul3.cinema.model.Seat;
import com.ftninformatika.jwd.modul3.cinema.web.dto.SeatDTO;

@Component
public class SeatToSeatDto implements Converter<Seat, SeatDTO> {

	@Autowired
	private HallToHallDto toHallDto;
	
	@Override
	public SeatDTO convert(Seat seat) {
		SeatDTO dto = new SeatDTO();
		
		dto.setId(seat.getId());
		dto.setHall(toHallDto.convert(seat.getHall()));
		
		return dto;
	}
	
	public List<SeatDTO> convert(List<Seat> seats){
        List<SeatDTO> seatsDto = new ArrayList<>();

        for(Seat seat : seats) {
            seatsDto.add(convert(seat));
        }

        return seatsDto;
    }

}
