package com.ftninformatika.jwd.modul3.cinema.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.modul3.cinema.model.Hall;
import com.ftninformatika.jwd.modul3.cinema.model.Seat;
import com.ftninformatika.jwd.modul3.cinema.service.SeatService;
import com.ftninformatika.jwd.modul3.cinema.support.SeatDtoToSeat;
import com.ftninformatika.jwd.modul3.cinema.support.SeatToSeatDto;
import com.ftninformatika.jwd.modul3.cinema.web.dto.HallDTO;
import com.ftninformatika.jwd.modul3.cinema.web.dto.SeatDTO;

@RestController
@RequestMapping(value = "/api/seats", produces = MediaType.APPLICATION_JSON_VALUE)
public class SeatController {

	@Autowired
	private SeatService service;
	
	@Autowired
	private SeatToSeatDto toSeatDto;
	
	@Autowired
	private SeatDtoToSeat toSeat;
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<SeatDTO> create (@RequestBody SeatDTO dto) {
		
		Seat seat = toSeat.convert(dto);
		Seat saved = service.save(seat);

		return new ResponseEntity<SeatDTO>(toSeatDto.convert(saved), HttpStatus.CREATED);
	}
	
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<SeatDTO> edit (@PathVariable Long id, @RequestBody SeatDTO dto) {
		
		if(!id.equals(dto.getId()))
			return new ResponseEntity<SeatDTO>(HttpStatus.BAD_REQUEST);
		
		Seat seat = toSeat.convert(dto);
		Seat saved = service.save(seat);
		
		return new ResponseEntity<SeatDTO>(toSeatDto.convert(saved), HttpStatus.CREATED);
	}
	
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Void> delete (@PathVariable Long id) {
		
		Seat deleted = service.delete(id);
		
		if(deleted != null) {
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}	
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<SeatDTO> getOne(@PathVariable Long id){
	   Seat seat = service.findOne(id);
	
	    if(seat != null) {
	        return new ResponseEntity<>(toSeatDto.convert(seat), HttpStatus.OK);
	    }else {
	        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    }
	}
	
	@GetMapping
	public ResponseEntity<List<SeatDTO>> getAll(){
	
	    List<Seat> seats = service.findAll();
	
	    return new ResponseEntity<>(toSeatDto.convert(seats), HttpStatus.OK);
	}
}
