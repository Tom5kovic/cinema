package com.ftninformatika.jwd.modul3.cinema.service.impl;

import com.ftninformatika.jwd.modul3.cinema.model.Adress;
import com.ftninformatika.jwd.modul3.cinema.repository.AdressRepository;
import com.ftninformatika.jwd.modul3.cinema.service.AdressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class JpaAdressService implements AdressService {
    @Autowired
    private AdressRepository adressRepository;

    @Override
    public Optional<Adress> findOne(Long id) {
        return adressRepository.findById(id);
    }

    @Override
    public List<Adress> findAll() {
        return adressRepository.findAll();
    }

    @Override
    public Adress save(Adress adress) {
        return adressRepository.save(adress);
    }

    @Override
    public void delete(Long id) {
        adressRepository.deleteById(id);
    }

}
