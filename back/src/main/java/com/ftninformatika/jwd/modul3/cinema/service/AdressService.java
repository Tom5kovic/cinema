package com.ftninformatika.jwd.modul3.cinema.service;

import com.ftninformatika.jwd.modul3.cinema.model.Adress;

import java.util.List;
import java.util.Optional;

public interface AdressService {

    Optional<Adress> findOne(Long id);

    List<Adress> findAll();

    Adress save(Adress adresa);

    void delete(Long id);

}
