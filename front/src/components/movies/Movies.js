import React from 'react';
import {Table, Button} from 'react-bootstrap'
import CinemaAxios from './../../apis/CinemaAxios';

class Movies extends React.Component {

    constructor(props) {
        super(props);

        this.state = { movies: []}
    }

    

    componentDidMount() {
        this.getMovies();
    }

    getMovies() {
        CinemaAxios.get('/movies')
            .then(res => {
                 console.log(res);
                 this.setState({movies: res.data});
            })
            .catch(error => {
                console.log(error);
                alert('Error occured please try again!');
            });
    }

    getGenresStringFromList(list) {
        return list.map(element => element.name).join(',');
    }

    renderMovies() {
        return this.state.movies.map((movie, index) => {
            return (
               <tr key={movie.id}>
                  <td>{movie.name}</td>
                  <td>{movie.duration}</td>
                  <td>{this.getGenresStringFromList(movie.genres)}</td>
                  <td><Button className="button button-navy" variant="success" onClick={() => this.goToEdit(movie.id)}>Edit</Button></td>
                  <td><Button className="button button-navy" variant="danger" onClick={() => this.delete(movie.id)}>Delete</Button></td>
               </tr>
            )
         })
    }

    goToEdit(movieId) {
        this.props.history.push('/movies/edit/'+ movieId); 
    }

    deleteFromState(movieId) {
        var movies = this.state.movies;
        movies.forEach((element, index) => {
            if (element.id === movieId) {
                movies.splice(index, 1);
                this.setState({movies: movies});
            }
        });
    }

    delete(movieId) {
        CinemaAxios.delete('/movies/' + movieId)
        .then(res => {
            console.log(res);
            alert('Movie was deleted successfully!');
            this.deleteFromState(movieId); 
        })
        .catch(error => {
            console.log(error);
            alert('Error occured please try again!');
         });
    }

    goToAdd() {
        this.props.history.push('/movies/add');  
    }

    render() {
        return (
            <div>
                <h1>Movies</h1>
                
                <div>
                    <Button className="button button-navy" onClick={() => this.goToAdd() }>Add</Button>
                    <br/>
                    
                    <Table id="movies-table" style={{marginTop:5}}>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Duration (min)</th>
                                <th>Genres</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.renderMovies()}
                        </tbody>                  
                    </Table>
                </div>
            </div>
        );
    }
}

export default Movies;