import React from 'react';
import CinemaAxios from './../../apis/CinemaAxios';
import {Button, InputGroup, Form} from 'react-bootstrap';
import SelectGenres from "./SelectGenres";

class EditMovie extends React.Component {

    constructor(props) {
        super(props);

        this.state = { movieId: -1, movieName: '', movieDuration: 0, showSelectGenres: false, selectedGenres: [] }

        this.edit = this.edit.bind(this);
        this.handleGenreSelection = this.handleGenreSelection.bind(this);
    }

    handleGenreSelection(selectedGenres) {
        this.setState({ selectedGenres: selectedGenres });
    }

    componentDidMount() {
       this.getMovieById(this.props.match.params.id);
    }

    getMovieById(movieId) {
        CinemaAxios.get('/movies/' + movieId)
        .then(res => {
            console.log(res);
            this.setState({movieId: res.data.id, movieName: res.data.name, movieDuration: res.data.duration});
        })
        .catch(error => {
            console.log(error);
            alert('Error occured please try again!');
         });
    }

    onNameChange = event => {
        console.log(event.target.value);

        const { name, value } = event.target;
        console.log(name + ", " + value);

        this.setState((state, props) => ({
            movieName: value
        }));
    }

    onDurationChange = event => {
        console.log(event.target.value);

        const { name, value } = event.target;
        console.log(name + ", " + value);

        this.setState((state, props) => ({
            movieDuration: value
        }));
    }

    edit() {
        var params = {
            'id': this.state.movieId,
            'name': this.state.movieName,
            'duration': this.state.movieDuration,
            'genres': this.state.selectedGenres
        };

        CinemaAxios.put('/movies/' + this.state.movieId, params)
        .then(res => {
            console.log(res);
            alert('Movie was edited successfully!');
            this.props.history.push('/movies');
        })
        .catch(error => {
            console.log(error);
            alert('Error occured please try again!');
         });
    }

    summarizedGenres(){
    
        return this.state.selectedGenres.map(element => element.name).join(',');
    }

    render() {
        return (
            <div>
                <h1>Edit movie</h1>
                <Form>
                    <Form.Group>
                        <Form.Label htmlFor="name">Name</Form.Label>
                        <Form.Control id="name" name="name" value={this.state.movieName} onChange={(e)=>this.onNameChange(e)} />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor="duration">Duration</Form.Label>
                        <Form.Control id="duration" name="duration" type="number" value={this.state.movieDuration} onChange={(e) => this.onDurationChange(e)}/>
                        
                        <Form.Label>Genres</Form.Label>
                            <InputGroup>
                                <Form.Control value={this.summarizedGenres()} disabled />
                            <InputGroup.Append>
                                <Button
                                    variant="info"
                                    onClick={() => this.setState({ showSelectGenres: true })}
                                >
                                 &gt;
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Form.Group>
                    <Button className="button button-navy" variant="success" onClick={() => this.edit()}>Edit</Button>
                </Form>

                <SelectGenres
                    show={this.state.showSelectGenres}
                    handleClose={() => this.setState({ showSelectGenres: false })}
                    selectedGenres={this.state.selectedGenres}
                    finishSelection={(newlySelectedGenres) =>
                        this.setState({
                        selectedGenres: newlySelectedGenres,
                        showSelectGenres: false,
                        })
                    }
                />
            </div>
        );
    }
}

export default EditMovie;