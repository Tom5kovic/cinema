import React from 'react';
import CinemaAxios from './../../apis/CinemaAxios';
import {Form, Button} from 'react-bootstrap';

class EditProjections extends React.Component {

    constructor(props){
        super(props);

        let projection = {
            time: "",
            type: "",
            hall: 0,
            price: 0.00,
            movie: null
        }

        this.state = {projection: projection, movies: []
            // , projectionId: -1, time: "", type: "",
            //          hall: 0, price: 0.00, movie: null 
                    };
    }

    valueInputChanged(e) {
        let input = e.target;
    
        let name = input.name;
        let value = input.value;
    
        let projection = this.state.projection;
        projection[name] = value;
    
        this.setState({ projection: projection });
      }

    movieSelectionChanged(e){
        console.log(e);

        let movieId = e.target.value;
        let movie = this.state.movies.find((movie) => movie.id == movieId);

        let projection = this.state.projection;
        projection.movie = movie;

        this.setState({projection: projection});
    }

    async edit(e) {
        e.preventDefault();

        try{

            let projection = this.state.projection;
            let projectionDTO = {
                dateTime: projection.time,
                movie: projection.movie,
                hall: projection.hall,
                type: projection.type,
                ticketPrice: projection.price
            }

            let response = await CinemaAxios.put("/projections", projectionDTO);
            this.props.history.push("/projections");
        }catch(error){
            alert("Couldn't edit the projection");
        }
    }

    // edit() {
    //     var params = {
    //         'id': this.state.projectionId,
    //         'dateTime': this.state.time,
    //         'hall': this.state.hall,
    //         'type': this.state.type,
    //         'ticketPrice': this.state.price,
    //         'movie': this.state.movie
    //     };

    //     CinemaAxios.put('/projections/' + this.state.projectionId, params)
    //     .then(res => {
    //         console.log(res);
    //         alert('Projection was edited successfully!');
    //         this.props.history.push('/projections');
    //     })
    //     .catch(error => {
    //         console.log(error);
    //         alert('Error occured please try again!');
    //      });
    // }

    render() {
        return(
            <div>
                <h1>Edit Projection</h1>

                <Form>
                    <Form.Group>
                        <Form.Label htmlFor="pTime">Time</Form.Label>
                        <Form.Control id="pTime" name="time" onChange={(e)=>this.valueInputChanged(e)} />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor="pType">Type</Form.Label>
                        <Form.Control id="pType" name="type" onChange={(e)=>this.valueInputChanged(e)} />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor="pHall">Hall</Form.Label>
                        <Form.Control type="number" id="pHall" name="hall" onChange={(e)=>this.valueInputChanged(e)} />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor="pPrice">Price</Form.Label>
                        <Form.Control type="number" step=".01" id="pPrice" name="price" onChange={(e)=>this.valueInputChanged(e)} />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label htmlFor="pMovie">Movies</Form.Label>
                        <Form.Control id="pMovie" name="time" onChange={(e)=>this.movieSelectionChanged(e)}>
                            <option></option>
                            {
                            this.state.movies.map((movie) => {
                                return (
                                    <option key={movie.id} value={movie.id}>{movie.name}</option>
                                )
                            })
                            }
                        </Form.Control>
                    </Form.Group>

                    <Button variant="success" onClick={(event)=>{this.edit(event);}}>Edit</Button>
                </Form>
            </div>
        );
    }
}

export default EditProjections;