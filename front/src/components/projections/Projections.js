import React from "react";
import CinemaAxios from "../../apis/CinemaAxios";

import {Button, Table, Form} from 'react-bootstrap';
import Avengers from '../../images/Avengers.jpg';
import "../../styles.css";

class Projections extends React.Component {
  constructor(props) {
    super(props);

    let params = {
      movieId: "",
      dateFrom: "",
    };

    this.state = { projections: [], movies: [], params: params, pageNum: 0, totalPages: 1};
  }

  componentDidMount() {
    this.getProjections();
    this.getMovies();
  }

  async getProjections() {
    try {

      let config = {params:{}};
      if(this.state.params.movieId != ""){
        config.params.movieId = this.state.params.movieId;
      }
      if(this.state.params.dateFrom != ""){
        config.params.dateTimeFromParameter = this.state.params.dateFrom;
      }

      config.params.pageNum = this.state.pageNum;

      let result = await CinemaAxios.get("/projections", config);
      this.setState({ projections: result.data, totalPages: result.headers["total-pages"]});
    } catch (error) {
      console.log(error);
    }
  }

  async getMovies(){
    try{
        let result = await CinemaAxios.get("/movies");
        let movies = result.data;
        this.setState({movies: movies});
    }catch(error){
        console.log(error);
        alert("Couldn't fetch movies");
    }
}

  goToAdd() {
    this.props.history.push("/projections/add");
  }

  goToEdit(projectionId) {
    this.props.history.push('/projections/edit/' + projectionId);
  }

  valueInputChanged(e){
    let control = e.target;

    let name = control.name;
    let value = control.value;

    let params = this.state.params;
    params[name] = value;

    this.setState({params: params});
  }

  doSearch(e){
    e.preventDefault();

    this.setState({pageNum: 0}, ()=>{this.getProjections();});
  }

  changePage(direction){
    let pageNum = this.state.pageNum;
    pageNum = pageNum + direction;

    this.setState({pageNum: pageNum}, ()=>{this.getProjections()});
  }

  deleteFromState(projectionId) {
    var projections = this.state.projections;
    projections.forEach((element, index) => {
        if (element.id === projectionId) {
          projections.splice(index, 1);
            this.setState({projections: projections});
        }
    });
  }

  delete(projectionId) {
    CinemaAxios.delete('/projections/' + projectionId)
    .then(res => {
        console.log(res);
        alert('Projection was deleted successfully!');
        this.deleteFromState(projectionId); 
    })
    .catch(error => {
        console.log(error);
        alert('Error occured please try again!');
     });
  }

  render() {
    return (
      <div>
        <h1>Projections</h1>

        <Form>
          <Form.Group>
            <Form.Label>Movies</Form.Label>
            <Form.Control onChange={(event)=>this.valueInputChanged(event)} name="movieId" as="select">
              <option value=""></option>
              {
                this.state.movies.map((movie) => {
                  return <option key={movie.id} value={movie.id}>{movie.name}</option>
                })
              }
            </Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Label>Date From</Form.Label>
            <Form.Control onChange={(e)=>{this.valueInputChanged(e);}} name="dateFrom"></Form.Control>
          </Form.Group>
          <Button style={{marginLeft:"1050px"}} variant="info" onClick={(e)=>this.doSearch(e)}>Search</Button>
        </Form>

        <div>
          <Button onClick={() => this.goToAdd()}>
            Add
          </Button>
          <br />

          <br/>
          <br/>

          <Table striped id="movies-table">
            <thead className="thead-dark">
              <tr>
                <th></th>
                <th>Movie Name</th>
                <th>Time</th>
                <th>Projection Type</th>
                <th>Hall</th>
                <th>Price</th>
              </tr>
            </thead>
            <tbody>
              {this.state.projections.map((projection) => {
                return (
                  <tr key={projection.id}>
                    <img alt={Avengers} src={Avengers} className="photo" />
                    <td>{projection.movie.name}</td>
                    <td>{projection.dateTime}</td>
                    <td>{projection.type}</td>
                    <td>{projection.hall.name}</td>
                    <td>{projection.ticketPrice}</td>
                    <td><Button className="button button-navy" variant="success" onClick={() => this.goToEdit(projection.id)}>Edit</Button></td>
                    <td><Button className="button button-navy" variant="danger" onClick={() => this.delete(projection.id)}>Delete</Button></td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
          <Button disabled={this.state.pageNum==0} onClick={()=>this.changePage(-1)}>Previous</Button>
          <Button disabled={this.state.pageNum == this.state.totalPages - 1} onClick={()=>this.changePage(1)}>Next</Button>
        </div>
      </div>
    );
  }
}

export default Projections;
