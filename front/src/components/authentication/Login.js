import React from "react";
import {login} from "../../services/auth";
import { Jumbotron, Form, Button } from 'react-bootstrap';

class Login extends React.Component {
  constructor() {
    super();

    this.state = { username: "", password: "" };
  }

  valueInputChange(event) {
    let control = event.target;

    let name = control.name;
    let value = control.value;

    let change = {};
    change[name] = value;
    this.setState(change);
  }

  doLogin(e){
    e.preventDefault();

    login(this.state.username, this.state.password);
  }

  render() {
    return (
      <div className='mt-5'>
        <Jumbotron>
          <h1>Welcome!</h1>
       <Form>
         <Form.Group>
           <Form.Label>Username</Form.Label>
           <Form.Control onChange={(e) => {this.valueInputChange(e);}} name="username" />
         </Form.Group>
         <Form.Group>
           <Form.Label>Password</Form.Label>
           <Form.Control onChange={(e) => {this.valueInputChange(e);}} type="password" name="password" />
         </Form.Group>
          <Button  onClick={(e) => {this.doLogin(e)}}>Log in</Button>
        </Form>
        </Jumbotron>
      </div>    
    );
  }
}

export default Login;
