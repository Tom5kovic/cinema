import axios from 'axios';

var CinemaAxios = axios.create({
  baseURL: 'http://localhost:8080/api',
  /* other custom settings */
});

export default CinemaAxios;
