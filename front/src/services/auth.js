import CinemaAxios from '../apis/CinemaAxios';
import jwt_decode from 'jwt-decode';

export const login = async function(username, password){

    try{
        let dto = {
            username: username,
            password: password
        };

        let result = await CinemaAxios.post("/users/auth", dto);
        console.log(result);

        let token = result.data;
        let decoded_token = jwt_decode(token);
        window.localStorage.setItem("token", token);
        window.localStorage.setItem("username", decoded_token.sub);
        window.location.reload();

    }catch(error){

    }
}


export const logout = function(){
    window.localStorage.removeItem("token");
    window.location.reload();
}